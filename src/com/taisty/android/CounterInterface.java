package com.taisty.android;

public interface CounterInterface {
	public void increaseCounter();
	public void decreaseCounter();
}
