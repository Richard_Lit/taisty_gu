package com.taisty.android;

import java.util.Arrays;
import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

public class MainActivity extends ActionBarActivity {
	
	private Button button;
	private Dialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.next_button);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				moveOn();
				
			}
		});
	}

	protected void moveOn() {
		 SharedPreferences settings = getSharedPreferences("ActivityPref", Context.MODE_PRIVATE);
		 if(settings.getBoolean("first_run", true)){
			 SharedPreferences.Editor editor = settings.edit();
		     editor.putBoolean("first_run", false);
		     editor.commit();
			 Intent intent = new Intent(this, SetupActivity.class);
			 startActivity(intent);
			 finish();
		 } else if(!settings.getBoolean("first_run", true)){
			 Intent intent = new Intent(this, RecipeActivity.class);
			 startActivity(intent);
			 finish();
		  }
	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void OnLoginClick(){
		progressDialog = ProgressDialog.show(MainActivity.this, "", "Logging in...", true);
	    
	    List<String> permissions = Arrays.asList("public_profile", "email");
	    // NOTE: for extended permissions, like "user_about_me", your app must be reviewed by the Facebook team
	    // (https://developers.facebook.com/docs/facebook-login/permissions/)
	    
	    ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
	      @Override
	      public void done(ParseUser user, ParseException err) {
	        progressDialog.dismiss();
	        if (user == null) {
	          Log.d(TaistyApplication.TAG, "Uh oh. The user cancelled the Facebook login.");
	        } else if (user.isNew()) {
	          Log.d(TaistyApplication.TAG, "User signed up and logged in through Facebook!");
	          button.setVisibility(View.VISIBLE);
	        } else {
	          Log.d(TaistyApplication.TAG, "User logged in through Facebook!");
	          button.setVisibility(View.VISIBLE);
	        }
	      }
	    });
	}
}
