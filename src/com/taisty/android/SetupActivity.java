package com.taisty.android;

import java.util.Locale;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;

public class SetupActivity extends ActionBarActivity implements CounterInterface {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	static int counter = 0;
	private CheckBox chkVeg;
	private Button btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setOffscreenPageLimit(7);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
            public void onPageSelected(int position) {
                // Log.d(TAG, "onPageSelected() :: " + "position: " + position);

                // skip fake page (first), go to last page
                if (position == 0) {
                    mViewPager.setCurrentItem(3, false);
                }

                // skip fake page (last), go to first page
                if (position == 4) {
                    mViewPager.setCurrentItem(1, false); //notice how this jumps to position 1, and not position 0. Position 0 is the fake page!
                }

            }

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
		});
		mViewPager.setCurrentItem(1, false);
		chkVeg = (CheckBox) findViewById(R.id.chkVeg);
		chkVeg.setBackgroundColor(Color.WHITE);
		chkVeg.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				//TODO handle onClick, set veggie as preferences
			}
		});
		btn = (Button) findViewById(R.id.button_setup_next);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(), RecipeActivity.class);
				startActivity(intent);
				finish();
				
			}
		});
		btn.setVisibility(View.GONE);
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setup, menu);
		return true;
	}
	
	// CounterInteface Methods
	@Override
	public void increaseCounter(){
		counter = counter + 1;
		if(counter==3){
			btn.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void decreaseCounter(){
		counter = counter - 1;
		if(counter<3){
			btn.setVisibility(View.GONE);
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).			
			switch(position){
			case 0: return PrefFragment.newInstance(position+1, R.drawable.meal1);
			case 1: return PrefFragment.newInstance(position+1, R.drawable.meal2);
			case 2: return PrefFragment.newInstance(position+1, R.drawable.meal3);
			default: return PrefFragment.newInstance(position + 1, R.drawable.meal1);
			}
		}

		@Override
		public int getCount() {
			// Show 3 actual and 2 dummy pages.
			return 5;
		}
		
		
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return getString(R.string.title_section4).toUpperCase(l);
			case 4:
				return getString(R.string.title_section5).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PrefFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";
		static final String STATE_state ="fragment_state";
		private ImageButton button;
		private int state;
		private int backgroundId;
		private CounterInterface mCounter;

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PrefFragment newInstance(int sectionNumber, int backId) {
			PrefFragment fragment = new PrefFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			args.putInt("ARG_BACKGROUND_ID", backId);
			fragment.setArguments(args);
			return fragment;
		}

		public PrefFragment() {
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState){
			mCounter = (CounterInterface) getActivity();
			super.onCreate(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_setup,
					container, false);
			rootView.setBackgroundResource(getArguments().getInt("ARG_BACKGROUND_ID"));
			button = (ImageButton) rootView.findViewById(R.id.imageButton1);
			if(savedInstanceState!=null){
				state = savedInstanceState.getInt("STATE_state");
				updateUi(state);
			} else {
				state = 0;
			}
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					state = (state+1)%3;
					updateUi(state);	
					if(state == 0){
						mCounter.decreaseCounter();
					} else if(state == 1){
						mCounter.increaseCounter();
					}
				}
			});
			
			return rootView;
		}
		
		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			savedInstanceState.putInt("STATE_state", state);
			super.onSaveInstanceState(savedInstanceState);
		}
		
		public void updateUi(int tempstate){
			switch(tempstate){
			case 0:
				button.setImageResource(R.drawable.android_transparent);
				break;
			case 1:
				button.setImageResource(R.drawable.android_checkmark);
				break;
			case 2:
				button.setImageResource(R.drawable.android_xmark);
				break;
			}
		}

		
	}
	
	

}
